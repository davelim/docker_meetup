WWTAL Showoff Themes is a collection of the CSS, JavaScript, and Showoff template files useful to theme a Showoff presentation. The intention is to match, as closely as is practical, the [company guidelines for branding presentation templates](https://sync.asynchrony.com/company/branding/).

Only "wide", 16x9 aspect ratio themes are available at this time.

**NOTE**: "Showme", a Ruby utility to manage Showoff theming, in the way homesick mangages dotfiles, is planned. In the meantime, these manual steps will do.

# Repo Location

This document anticipates showme to function similarly to homesick. Therefore, it assumes the wttal-showoff-themes repository was cloned under:

```
~/.showme/repos
```

# Theme Links

Two shell scripts, _light.sh_ and _dark.sh_, are provided to help manage quickly switching between 16x9 light theme and 16x9 dark theme.

From your presentation directory, where showoff.json is located, do the following.

* Simlink light.sh into your project directory

  ```
  ln -s ~/.showme/repos/wwtal-showoff-themes/light.sh

  ```
* Simlink dark.sh into your project directory

  ```
  ln -s ~/.showme/repos/wwtal-showoff-themes/dark.sh

  ```
* Run either script to setup symlinks that allow Showoff to detect your chosen theme.

# Templates

The set of templates in the project include a default slide format that should work well for quickly getting up and running with Showoff themed for WWT Asynchrony Labs. Other template are also provided for more elaborate slide formats. See **Usage** section below for more

## Setup

>**IMPORTANT**  Due to restrictions with Showoffs hooks, and lack of priority to fix :), presentations using these themes need to include at least one slide using the default template.

Include the following JSON in the showoff.json for your presentation.

```json
"templates": {
  "splash": "./theme-root/templates/splash.tpl",
  "chapter-title": "./theme-root/templates/chapter-title.tpl",
  "single-image": "./theme-root/templates/single-image.tpl",
  "slash-emphasis": "./theme-root/templates/slash-emphasis.tpl",
  "default": "./theme-root/templates/default.tpl"
}
```

## Default

Showoff will select the default template if no other template is specified.

![default](documentation/default.tpl.png)

```
!SLIDE
# Default Template
## Wants A Subtitle Too

### My Fellow Asynchronites

* Ask not what your team can do for you
* Ask what you can do for your team
```

### Transformations
* First h1 - moved to header block
* First h2 - moved to header block
* If no h1 and no h2, header border will not appear
* All other content appears below header border

## Splash

![splash](documentation/splash.tpl.png)

```
!SLIDE[tpl=splash]

# Showoff Theming
## An exploration in CSS and JavaScript
```

### Transformations

* h1 - large, centered text
* h2 - smaller text next to line
* line stretches to fit
* other content ignored

## Chapter Title
> This template does not yet fully match branding example

![chapter title](documentation/chapter-title.tpl.png)

```
# The First
## 1

![ribbon](blue-ribbon.png)

There can be only one, one.
```

### Transformations

* h1 - large text below triple slashes
* h2 - text below dividing line at bottom of circle
* p - text in center of circle
* img - set as background of slide


## Single Image

![single image](documentation/single-image.tpl.png)

```
!SLIDE[tpl=single-image]

# Asynchrony St Louis Office
## The intersection of historic and modern

![lobby](asynchrony-lobby.jpg)
```

### Transformations

* First h1 - moved to header block
* First h2 - moved to header block
* img - set into matte box
* other content ignored

## Slash Emphasis

![slash emphasis](documentation/slash-emphasis.tpl.png)

```
!SLIDE[tpl=slash-emphasis]

# All Your Bases Are Belong To Us

Because we're all about dat base, 'bout dat base
```

### Transformations

* First h1 - set as lower, larger text
* First p - set as middle, smaller text
* other content ignored
