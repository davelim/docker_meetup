var showoffHelper = {};
showoffHelper.handleShow = function(event) {

    function dashToCamelCase(templateName) {
        return templateName.replace( /-([a-z])/gi, function ( $0, $1 ) { return $1.toUpperCase(); } );
    }

    function preventShowoffBlockDisplay($target) {
        // Prevent showoff from forcing the slide into block display
        $target.parents('.slide').css('display', '');
    }

    var templates = {};

    templates.chapterTitle = function($target, $targetDiv, $holder) {
        var $img = $holder.find('img').detach();
        $target.css('background-image', 'url("' + $img.attr('src') + '")');

        var $titleWrapper = $target.find('.title-wrapper');

        $holder.find('h1').detach().appendTo($titleWrapper);
        $holder.find('p:first-child').detach();
        $holder.find('p:last-child').detach().appendTo($titleWrapper);
        $('<hr/>').appendTo($titleWrapper);
        $holder.find('h2').detach().appendTo($titleWrapper);
    };

    templates.slashEmphasis = function($target, $targetDiv, $holder) {
        // Remove showoff's extra p
        $targetDiv.find('.content p').detach();


        $holder.find('p').detach().appendTo($targetDiv);
        $holder.find('h1').detach().appendTo($targetDiv);
    };

    templates.default = function($target, $targetDiv, $holder) {
        var $defaultHeader = $targetDiv.find('.default-header');

        var $h1 = $targetDiv.find('h1').detach();
        $h1.appendTo($defaultHeader);

        var $h2 = $targetDiv.find('h2').detach();
        $h2.appendTo($defaultHeader);
    };

    templates.splash = function($target, $targetDiv, $holder) {
        var $h1 = $holder.find('h1').detach();
        var $splashSubtitleWrapper =  $targetDiv.find('.splash-subtitle-wrapper');
        $h1.insertBefore($splashSubtitleWrapper);

        var $h2 =  $holder.find('h2').detach();
        $h2.appendTo($splashSubtitleWrapper);
    };

    templates.singleImage = function($target, $targetDiv, $holder) {
        var $singleImageHeader = $targetDiv.find('.single-image-header');
        var savedDisplay = $singleImageHeader.css('display');
        $singleImageHeader.css('display', 'none');

        var $h1 = $holder.find('h1').detach();
        $h1.appendTo($singleImageHeader);

        var $h2 = $holder.find('h2').detach();
        $h2.appendTo($singleImageHeader);

        if ($h1.length > 0 || $h2.length > 0) {
            $singleImageHeader.css('display', savedDisplay);
        }

        var $img = $holder.find('img').detach();
        $img.appendTo($targetContentDiv.find('.single-image-wrapper'));
    };

    templates.doubleImage = function($target, $targetDiv, $holder) {
        var $singleImageHeader = $targetDiv.find('.double-image-header');
        var savedDisplay = $singleImageHeader.css('display');
        $singleImageHeader.css('display', 'none');

        var $h1 = $holder.find('h1').detach();
        $h1.appendTo($singleImageHeader);

        var $h2 = $holder.find('h2').detach();
        $h2.appendTo($singleImageHeader);

        if ($h1.length > 0 || $h2.length > 0) {
            $singleImageHeader.css('display', savedDisplay);
        }

        var $imageWrapper = $targetContentDiv.find('.double-image-wrapper');
        var imageCounter = 0;
        $holder.find('img').detach().each(function($index, $img) {
            var $imgDiv = $('<div class="img-holder"></div>');

            $imgDiv.append($img).appendTo($imageWrapper);
            imageCounter += 1;
        });
        console.log("image count=" + imageCounter);

        $imageWrapper.addClass("image-count-" + imageCounter);
    };

    $target = $(event.target);
    if ($target.attr('data-processed') !== "true") {
        var $targetContentDiv = $target.find('[data-template]');
        var template = $targetContentDiv.attr('data-template');
        $target.parents('.slide').addClass('tpl-' + template);

        var $holder = $targetContentDiv.siblings('.holder');

        var templateFunctionName = dashToCamelCase(template);

        if (templates[templateFunctionName] !== undefined) {
            templates[templateFunctionName]($target, $targetContentDiv, $holder);
        }

        $target.attr('data-processed', true);
    }

    preventShowoffBlockDisplay($target);
};
