<!SLIDE >
# how / overview
## darts ci pipeline
| devs          | QA | jenkins       | container-builders | maestro  |
| ------------- |----|-------------|--------------|-------------------|
| pushes a new feature branch | pairing | detects branch and creates subproject |||
| first commit to branch | pairing | detects commit, runs partial jenkinsfile | build app containers from dockerfiles, and push to registry ||
| further commits | pairing | detects commit, runs entire jenkinsfile | update app containers and push | runs docker-compose up to update branch app stack |
| marks feature dev done | manual and exploritory test against branch stack |||
| merge to master | marks feature complete and correct | runs master branch project | updates app containers and updates cf-app target* | removes branch app stack, updates master stack, pushes app to cloud foundry* |

~~~SECTION:notes~~~
* denote pipeline features currently work in progress. Cloud Foundry is the   
ultimate delivery environment at WWT. WWT has it's own pipeline once apps are   
uploaded to their dev cloud foundry instance
~~~ENDSECTION~~~

<!SLIDE >
# how / demo
## darts ci pipeline
* Jenkins Mutlibranch project
  * Jenkinsfile
* App build and release containers
  * Dockerfiles
	* Docker Registry
* Maestro and app stack templating
  * docker-compose
	* Rancher   

_Interact!_ Ask questions and share experiences.
	~~~SECTION:notes~~~
	Demo - Walk through each dev action (flip between slides) and show the result  
	in Jenkins and Rancher
	~~~ENDSECTION~~~

<!SLIDE >
# how / jenkins
## jenkins multibranch pipeline project
Jenkins watches a given repository for new branches. Per push of a new branch that
meets designated criteria (regular expression match and must have Jenkinsfile),
it will create a new subproject for you.   

Each subproject has it's own pipeline view in Jenkins, which shows granular
successes,failures, and time spent per step in your defined pipeline.   

<!SLIDE >
# how / jenkins
## Jenkinsfile
Jenkinsfiles are Jenkin's version of a Makefile, except for your automation
pipeline. It contains all of the SCM, build, test and deploy steps for a project,
completely *code*. Not a crippled DSL, but actual Groovy code that is compiled
and executed.   

Each branch has it's own Jenkinsfile -- you can make changes and merges to this file
like you would any other code.

Because Jenkinsfiles are Groovy code, you have access to RESTful APIs, OS level
interaction (including Docker) to drive automation decisions and actions. Complex
delivery pipelines are first class priority in Jenkins 2.
~~~SECTION:notes~~~
Big takeaway for me here -- You builds are now trackable artifacts. You are also   
not limited by Jenkins plugins to enable the functionality you require.   

Demo - Create a new branch and push

<!SLIDE >
# how / docker
## dockerfiles
Dockerfiles are Docker's version of a Makefile, except they contain instructions
on how to build a custom container, derived from any given base container.   

Each instruction in the file is executed on top of the base container image
and creates a new layer or, an *intermediary* container. Those layers are then
squashed down to create a fully-fledged working container.

Like Make (sort of), Docker is smart enough to know what lines in Dockerfiles
have changed, and what external file dependencies have changed. It'll therefore
only rebuild what is needed when you make changes.

<!SLIDE >
# how / docker
## dockerfiles
Takeaway: Any Dockerized infrastructure services, whether it be for development
environments, prod, or your app itself, can be completely described in text and
source-control managed.

*Opinion:* Infrastructure as code doesn't always completely eliminate the need
for other CM (configuration management) tools, but it can greatly simplify it.
~~~SECTION:notes~~~
Big takeaway for me here -- Any development and production services .   

Demo - Create a new branch and push

<!SLIDE >
# how / docker
## docker-compose
docker-compose is a new Docker provided app that allows you to describe entire
application stacks in a file. The stack can have multiple containers, multiple
networks, and start up dependencies. Docker-compose is aware
of what containers have changed, and will do a rolling upgrade of a running
stack.
