<!SLIDE[tpl=splash] >
# ci/cd in practice
## docker saint louis

<!SLIDE >
# intro
## welcome!
Welcome to our first Meetup of the year! We will be discussing and demoing a
live use case of Docker in our Dev and QA environment.   

* Schedule
  * 7:00pm - 8:15pm Presentation/Demo
  * 8:15pm - 8:30pm Break
  * 8:15pm - 9:00pm Lean Coffee
* Rules
  * Raise hands and ask questions
  * Ask more questions!
  * Share stories

~~~SECTION:notes~~~
Assuming some prior knowlege of Docker and CI/CD in practice. If these are new
practical examples may help

Intro from Shelly
~~~ENDSECTION~~~
