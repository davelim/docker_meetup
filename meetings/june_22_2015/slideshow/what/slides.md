<!SLIDE >
# what / the project
## DARTS
<img src="../_images/darts.png" width="20%" height="20%">   

The DARTS Project (Dock Appointment and Receiving Tracking System) is an joint
project between Asynchrony and WWT to deliver a mobile-ready web app of epic
proportions to WWT's ITC logistics team in Edwardsville, IL.  

It will be used to manage dock appointments, track shipment inventory, and gather
metrics to rate carriers on their performance.   

~~~SECTION:notes~~~
Uses NodeJS and AngularJS.
~~~ENDSECTION~~~

<!SLIDE >
# what / docker
Docker is an open container format that allows a host machine to run multiple,
self-contained* guest-OSs without the need of a hypervisor   

Benefits:   

* Lightweight, performant *(jargon)*, and cheap
* Reusable and community driven containers
* Extendable driver interfaces to support 3rd-party virtual networking and
file systems
* Rich APIs to support infinite automation and orchestration strategies

~~~SECTION:notes~~~
* It's not *entirely* self-contained and there are some implications to How
Docker does it's magic with the Kernel
~~~ENDSECTION~~~

<!SLIDE >
# what / jenkins
<img src="../_images/jenkins.png">   

Jenkins is the leading open-source automation server. It supports highly
flexible automation piplelines, is source control agnostic, and enables
horizontal scaling to support heavy workloads.   

Jenkins 2 brings us*:

* The ability to describe almost everything Jenkins does, in
code.
* Increased visibility into our complex and asynchronous CI/CD piplelines.

~~~SECTION:notes~~~
* This functionality is available to an extent in Jenkins 1.8 as plugins
~~~ENDSECTION~~~

<!SLIDE >
# what / rancher
<img src="../_images/rancher.jpeg" width="35%" heigh="35%">   

Rancher is a web based portal that provides a rich graphical interface for managing
a container based infrastructure. It wraps production infrastructure necessities
(load-balancers, service discovery, auto-scaling, health checks) around
otherwise vanilla Docker containers.   

Rancher also natively supports multiple Docker orchestrators, including Docker
Swarm, MESOS, and Kubernetes without abstracting away what makes them unique. It
also now supports native KVM virtual machine management.
